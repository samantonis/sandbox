module.exports = function (grunt) {
	grunt.initConfig({
		sass: {
			dist: {
				files: {
					'public/stylesheets/style.css': 'sass/main.scss'
				}
			}
		},

		watch: {
			css: {
				files: ['sass/**/*.scss'],
				tasks: ['sass'],
				options: {
					livereload: true // needed to run LiveReload
				}
			},
			js: {
				files: ['modules/**/*.js'],
				tasks: ['concat', 'uglify'],
				options: {
					livereload: true // needed to run LiveReload
				}
			}
		},

		concat: {
			dist: {
				// the files to concatenate
				src: ['modules/**/*.js'],
				// the location of the resulting JS file
				dest: 'main.js'
			}
		},

		uglify: {
			options: {
				// the banner is inserted at the top of the output
				banner: '/*! minified js %> */\n'
			},
			dist: {
				files: {
					'public/js/main.min.js': ['<%= concat.dist.dest %>']
				}
			}
		},

		scsslint: {
			allFiles: [
				'sass/**/*.scss'
			],
			options: {
				config: 'scss-lint.yml',
				reporterOutput: 'scss-lint-report.xml',
				colorizeOutput: true,
				exclude: [
					'sass/fontawesome/*.scss',
					'sass/_normalize.scss',
					'sass/base/_fonts.scss'
				]
			}
		},

		assemble: {
			options: {
				assets: 'assets',
				plugins: ['permalinks'],
				partials: ['./views/includes/**/*.html'],
				layout: ['./views/home.html']
			},
			site: {
				src: ['./views/*.html'],
				dest: './public/'
			}
		}

	});

	//load dependencies
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-assemble');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-scss-lint');

	// specify tasks to run with grunt command
	grunt.registerTask('default', ['concat', 'sass', 'uglify', 'watch']);
};