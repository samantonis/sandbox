////////////////////////////////////////////////////////////////////////////////
//
// _DEFAULTS.SCSS
//
// Declare all defaults like body, headings, paragraphs, lists
// You don't use classes here!
//
////////////////////////////////////////////////////////////////////////////////

html {
	height: 100%;
	background-color: $bg-color;
	font-family: $georgia;
	box-sizing: border-box;
}

body {
	position: relative;
	min-height: 100%;
	margin: 0;
	padding-bottom: 6rem;
	color: $text-color;
	font-size: $base-font-size;
	line-height: $base-line-height;
}

h1,
h2,
h3,
h4,
h5,
h6 {
	margin-top: 0;
	font-style: normal;
	font-weight: lighter;
	line-height: $base-line-height;
}

h1 {
	font-size: $h2-size;

	@include mq-min($bp1) {
		font-size: $h1-size;
	}
}

h2 {
	font-size: $h3-size;

	@include mq-min($bp1) {
		font-size: $h2-size;
	}
}

h3 {
	font-size: $h4-size;

	@include mq-min($bp1) {
		font-size: $h3-size;
	}
}

h4 {
	font-size: $h5-size;

	@include mq-min($bp1) {
		font-size: $h4-size;
	}
}

h5 {
	font-size: $h6-size;

	@include mq-min($bp1) {
		font-size: $h5-size;
	}
}

h6 {
	font-size: $h6-size;

	@include mq-min($bp1) {
		font-size: $h6-size;
	}
}

img {
	display: block;
	width: auto \9;
	max-width: 100%;
	height: auto;
	vertical-align: middle;
}

textarea {
	padding: 5px;
	border: 1px solid $black;
	border-radius: 4px;
	resize: none;
}

a {
	color: $black;
	text-decoration: none;

	&:focus,
	&:hover {
		color: $steel;
	}
}

ul {
	padding: 0;
}

li {
	margin-bottom: 5px;
	list-style-type: none;
}

hr {
	height: 2px;
	border: 0;
	background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, .75), rgba(0, 0, 0, 0));
}

input {
	padding: 5px;
	border: 1px solid $black;
	border-radius: 4px;
}

footer {
	position: absolute;
	right: 0;
	bottom: 0;
	left: 0;
	min-height: 40px;
	padding: $half-spacing-unit;
	background-color: $black;
	color: $white;
}
