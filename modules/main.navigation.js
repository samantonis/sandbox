App.modules.navigation = (function () {

	var init = function () {
		var icon = $('.hamburger');
		icon.on('click', toggle);
	};

	function toggle() {
		var nav = $('.nav__links--mobile');
		if (nav.hasClass('is-hidden')) {
			nav.removeClass('is-hidden');
		} else {
			nav.addClass('is-hidden');
		}
	}


	return {
		init: init
	};

})();